CREATE DATABASE "eb_post";

CREATE SCHEMA IF NOT EXISTS "develop";

CREATE TABLE IF NOT EXISTS "develop"."post" (
    "id" varchar PRIMARY KEY,
    "user_id" varchar UNIQUE NOT NULL,
    "content" varchar,
    "image" varchar NOT NULL,
    "likes" integer DEFAULT 0,
    "comments" json,
    "date_created" timestamp DEFAULT CURRENT_TIMESTAMP
);